package kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SpeedService {

    @Autowired
    private Sender sender;

    @Autowired
    private KafkaTemplate<String, OwnerRecord> kafkaTemplateToFast;

    Map<String, SensorRecord> cars = new HashMap<>();

    @KafkaListener(topics = {"cameratopic1"})
    public void receive(@Payload SensorRecord SensorRecord,
                        @Headers MessageHeaders headers) {


        cars.put(SensorRecord.getLicencePlate(), SensorRecord);
        System.out.println("Speed 1: License plate: " + SensorRecord.getLicencePlate() + ", time: "
                + SensorRecord.getMinute() + ":" + SensorRecord.getSecond());

    }

    @KafkaListener(topics = { "cameratopic2"})
    public void receive2(@Payload SensorRecord SensorRecord,
                        @Headers MessageHeaders headers) {

        System.out.println("Speed 2: License plate: " + SensorRecord.getLicencePlate() + ", time: "
                + SensorRecord.getMinute() + ":" + SensorRecord.getSecond());

        SensorRecord sensor1 = cars.get(SensorRecord.getLicencePlate());
        if(sensor1 == null) return;

        int seconds = ((SensorRecord.getMinute() - sensor1.getMinute())*60-sensor1.getSecond() + SensorRecord.getSecond());
        double speed = (0.5/seconds)*3600;
        if(speed <=0) return;
        
        System.out.println("License plate: " + SensorRecord.getLicencePlate() + ", speed: " + (int) speed);

        if(speed > 72){
            kafkaTemplateToFast.send("toFastTopic", new OwnerRecord("Owner of " + SensorRecord.getLicencePlate(), (int) speed, SensorRecord.getLicencePlate()));
        }
    }
}
