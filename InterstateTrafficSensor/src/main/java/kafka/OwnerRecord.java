package kafka;


public class OwnerRecord {

    String name;
    int speed;
    public String licencePlate;

    public OwnerRecord(String name, int speed, String licencePlate) {
        super();
        this.name = name;
        this.speed = speed;
        this.licencePlate = licencePlate;
    }
    public OwnerRecord(){
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }
}
