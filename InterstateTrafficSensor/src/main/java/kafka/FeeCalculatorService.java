package kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class FeeCalculatorService {

    @KafkaListener(topics = {"toFastTopic"})
    public void receive(@Payload OwnerRecord ownerRecord,
                        @Headers MessageHeaders headers) {

        int speed = ownerRecord.getSpeed();
        int fine = 25;
        if(speed > 77 && speed <= 82){
            fine = 45;
        } else if(speed > 82 && speed <= 90){
            fine = 80;
        } else if(speed > 90){
            fine = 125;
        }

        System.out.println("Owner info: "+ ownerRecord.getName() + ", fine: " + fine);

    }
}
